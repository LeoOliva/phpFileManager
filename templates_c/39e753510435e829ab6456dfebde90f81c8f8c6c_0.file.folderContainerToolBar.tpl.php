<?php
/* Smarty version 3.1.29, created on 2016-07-30 21:56:22
  from "/home/oliva/git/php/phpFileManager/templates/folderContainerToolBar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_579d68d6ce0cc0_24791182',
  'file_dependency' => 
  array (
    '39e753510435e829ab6456dfebde90f81c8f8c6c' => 
    array (
      0 => '/home/oliva/git/php/phpFileManager/templates/folderContainerToolBar.tpl',
      1 => 1469933781,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_579d68d6ce0cc0_24791182 ($_smarty_tpl) {
?>
<div id="folderContainerNavNar">
    <ul class="nav">
        <li>
            <div id="folderContainerToolBarDropdown1" class="dropdown">
                <a href="#" 
                   class="dropdown-toggle" 
                   data-toggle="dropdown" 
                   role="button" 
                   aria-haspopup="true" 
                   aria-expanded="false">Select 
                   <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="#">All</a></li>
                  <li><a href="#">None</a></li>
                  <li><a href="#">Inverse</a></li>
                </ul>
            </div>
        </li>
        <li><a>Delete</a></li>
        <li><a>Rename</a></li>
        <li><a>New folder</a></li>
        <li><a>Compress</a></li>
        <li><a>Download</a></li>
        <li><a>Upload</a></li>
    </ul>
</div><?php }
}
