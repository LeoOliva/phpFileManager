<?php
/* Smarty version 3.1.29, created on 2016-08-03 13:41:01
  from "/home/oliva/git/php/phpFileManager/templates/folderContainer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57a23abdd3b490_95054933',
  'file_dependency' => 
  array (
    '2ec5080ec0dc4b3b44e33b555173b98e12065f30' => 
    array (
      0 => '/home/oliva/git/php/phpFileManager/templates/folderContainer.tpl',
      1 => 1470249658,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57a23abdd3b490_95054933 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'smarty/libs/plugins/modifier.date_format.php';
?>
<div id="folderContainer">
 
    <table id="folderContainerTableElements">
        <tr>
            <th>Name</th>
            <th>Size</th>
            <th>Date</th>
            <th>Type</th>
            
        </tr>
            <?php
$_from = $_smarty_tpl->tpl_vars['folderContent']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_content_0_saved_item = isset($_smarty_tpl->tpl_vars['content']) ? $_smarty_tpl->tpl_vars['content'] : false;
$_smarty_tpl->tpl_vars['content'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['content']->index=-1;
$_smarty_tpl->tpl_vars['content']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
$_smarty_tpl->tpl_vars['content']->_loop = true;
$_smarty_tpl->tpl_vars['content']->index++;
$__foreach_content_0_saved_local_item = $_smarty_tpl->tpl_vars['content'];
?>
                <tr id="<?php echo $_smarty_tpl->tpl_vars['content']->value->getFullName();?>
" title="<?php echo $_smarty_tpl->tpl_vars['content']->value->getFullName();?>
" class="<?php if ((1 & $_smarty_tpl->tpl_vars['content']->index)) {?>colorEven1<?php } else { ?>colorOdd1<?php }?>">
                    <!-- Name -->
                    <td> <?php echo $_smarty_tpl->tpl_vars['content']->value->getFullName();?>
 </td>
                    <!-- Size -->
                    <td>
                        <?php if ($_smarty_tpl->tpl_vars['content']->value->isFile()) {?>
                                <?php echo $_smarty_tpl->tpl_vars['content']->value->getFileSize();?>

                           <?php } else { ?>
                                ---
                       <?php }?>
                    </td>
                    <!-- Date -->
                    <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['content']->value->getDateLastModification(),"%d/%m/%Y %H:%M");?>
</td>
                    <!-- Type -->
                    <td>
                       <?php if ($_smarty_tpl->tpl_vars['content']->value->isFile()) {?>
                                <?php echo $_smarty_tpl->tpl_vars['content']->value->getFileExtension();?>

                           <?php } else { ?>
                                Directory
                       <?php }?>
                    </td>
                </tr>
                
            <?php
$_smarty_tpl->tpl_vars['content'] = $__foreach_content_0_saved_local_item;
}
if ($__foreach_content_0_saved_item) {
$_smarty_tpl->tpl_vars['content'] = $__foreach_content_0_saved_item;
}
?>
    </table>
</div>
    
  <?php }
}
