<?php
/* Smarty version 3.1.29, created on 2016-07-30 22:27:04
  from "/home/oliva/git/php/phpFileManager/templates/leftSideBar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_579d7008482590_89725962',
  'file_dependency' => 
  array (
    '1d4c2ea96932bda543ba38898c7d3a629979286d' => 
    array (
      0 => '/home/oliva/git/php/phpFileManager/templates/leftSideBar.tpl',
      1 => 1469935622,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_579d7008482590_89725962 ($_smarty_tpl) {
?>
<div id="treeSideBar">
    <ul class="nav">
                <?php
$_from = $_smarty_tpl->tpl_vars['folderContent']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_content_0_saved_item = isset($_smarty_tpl->tpl_vars['content']) ? $_smarty_tpl->tpl_vars['content'] : false;
$_smarty_tpl->tpl_vars['content'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['content']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
$_smarty_tpl->tpl_vars['content']->_loop = true;
$__foreach_content_0_saved_local_item = $_smarty_tpl->tpl_vars['content'];
?>
                    <li id="<?php echo $_smarty_tpl->tpl_vars['content']->value->getFullName();?>
" title="<?php echo $_smarty_tpl->tpl_vars['content']->value->getFullName();?>
">
                           <?php if ($_smarty_tpl->tpl_vars['content']->value->isDir()) {?>
                               <a href="#"><?php echo $_smarty_tpl->tpl_vars['content']->value->getCutName();?>
</a>
                           <?php }?>
                    </li>
                <?php
$_smarty_tpl->tpl_vars['content'] = $__foreach_content_0_saved_local_item;
}
if ($__foreach_content_0_saved_item) {
$_smarty_tpl->tpl_vars['content'] = $__foreach_content_0_saved_item;
}
?>
    </ul>
</div><?php }
}
