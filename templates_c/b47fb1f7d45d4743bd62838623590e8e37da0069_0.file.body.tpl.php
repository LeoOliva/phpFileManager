<?php
/* Smarty version 3.1.29, created on 2016-08-05 20:32:01
  from "/home/oliva/git/php/phpFileManager/templates/body.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57a53e11051ef3_08966784',
  'file_dependency' => 
  array (
    'b47fb1f7d45d4743bd62838623590e8e37da0069' => 
    array (
      0 => '/home/oliva/git/php/phpFileManager/templates/body.tpl',
      1 => 1470367555,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:folderContainerToolBar.tpl' => 1,
    'file:leftSideBar.tpl' => 1,
    'file:serverInfo.tpl' => 1,
    'file:folderContainer.tpl' => 1,
  ),
),false)) {
function content_57a53e11051ef3_08966784 ($_smarty_tpl) {
?>
<body>
    <!--
    Extra small (for smartphones .col-xs-*), 
    small (for tablets .col-sm-*), 
    medium (for laptops .col-md-*) 
    large (for laptops/desktops .col-lg-*). 
    These grid sizes enable you to control grid behavior 
    on different devices, i.e. desktop, laptops, tablet, smartphone, etc.
    -->
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-left">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">1</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">2</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">3</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">4</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">5</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">6</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">7</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">8</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">9</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">10</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">11</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">12</div>
            <br>
            <br>
            
            
             <div id="sidebar" class="col-lg-3 col-xs-10 sidebar-offcanvas"  role="navigation">
                 <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:folderContainerToolBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                 <!-- <hr> -->
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:leftSideBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <!-- <hr> -->
            </div>
            <div id="content" class="col-lg-9 col-xs-10">
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:serverInfo.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:folderContainer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 
            </div>
        </div>
    </div>
    
    
    
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><?php echo '</script'; ?>
>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <?php echo '<script'; ?>
 src="js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="js/customView.js"><?php echo '</script'; ?>
>
    
</body><?php }
}
