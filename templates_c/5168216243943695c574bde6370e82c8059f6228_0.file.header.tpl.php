<?php
/* Smarty version 3.1.29, created on 2016-07-28 21:14:57
  from "/home/oliva/git/php/phpFileManager/templates/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_579abc2195d949_10248676',
  'file_dependency' => 
  array (
    '5168216243943695c574bde6370e82c8059f6228' => 
    array (
      0 => '/home/oliva/git/php/phpFileManager/templates/header.tpl',
      1 => 1469758495,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_579abc2195d949_10248676 ($_smarty_tpl) {
?>
<header>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
   
    
    <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['title']->value, 'UTF-8');?>
</title> 
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
      <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    
  
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                
                 <button type="button" 
                         class="navbar-toggle collapsed" 
                         data-toggle="collapse" 
                         data-target="#navbar-collapse-1" 
                         aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                
                  <a class="navbar-brand" href="#">File Browser</a>
            </div>
           
            <form class="navbar-form navbar-left" role="search" action="#">
                <div class="form-group">
                    <input type="search" class="form-control" placeholder="Search" >                        
                </div>
            </form>
            
            <div id="navbar-collapse-1" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Quick Access <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                            </ul>
                    </li>
                </ul>
                <ul id="pathBreadcrumbs" class="nav navbar-nav">
                    <li><a href="#">/</a></li>    
                    <li><a href="#">1/</a></li>    
                    <li><a href="#">2/</a></li>    
                    <li><a href="#">3/</a></li>    
                    <li><a href="#">4/</a></li>    
                    <li><a href="#">5/</a></li>    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">About me</a></li>
                </ul>
            </div>
            
        </div>
    </nav>
    
  
</header>
    <?php }
}
