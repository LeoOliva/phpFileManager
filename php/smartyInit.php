<?php

/* 
 * Leonardo Oliva
 * 
 * Creating a Smarty instance
 * 
 */



//We defined a constant
define('SMARTY_DIR', 'smarty/libs/');
//Included Smarty lib.
require(SMARTY_DIR . 'Smarty.class.php');
//Creating a Smarty object
$smarty = new Smarty;

//Settings

//The paths could be  relative or absolute; It is no necessary
$smarty->template_dir = 'templates/';
$smarty->compile_dir = 'templates_c/';
$smarty->config_dir = 'configs/';
$smarty->cache_dir = 'cache/';
        

