<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of element
 *
 * @author oliva
 */

/**
 * <?php
$partes_ruta = pathinfo('/www/htdocs/inc/lib.inc.php');

echo $partes_ruta['dirname'], "\n";
echo $partes_ruta['basename'], "\n";
echo $partes_ruta['extension'], "\n";
echo $partes_ruta['filename'], "\n"; // desde PHP 5.2.0
?>
 */

class element {
    
    //Propierties
    private $pathInfo;
    private $fullPath;
    private $stat; //http://php.net/manual/es/function.stat.php
    
    //Methods
    public function  __construct($fullPath){
        if(is_dir($fullPath) && $fullPath[strlen($fullPath) - 1] != '/'){
            $fullPath .= '/';
        }else{
            //$stat = stat($fullPath);
        }
        
        @$this->stat = stat($fullPath);
        
        $this->pathInfo = pathinfo($fullPath);
        $this->fullPath = $fullPath;
    }
    
    public function getFullName(){
        return $this->pathInfo['basename'];
    }
    
    public function getDirName(){
        return $this->pathInfo['dirname'] . '/';
    }
    
    public function getFileName(){
        return $this->pathInfo['filename'];
    }
    
    public function getFullPath(){
        return $this->fullPath;
    }
    
    public function getFileSize(){
        return $this->human_filesize(filesize($this->getFullPath()));
    }
    
    function human_filesize($bytes, $decimals = 2) {
        //http://php.net/manual/es/function.filesize.php
        
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
         
      }
      /**
       * 
       * function HumanSize($Bytes)
{
  $Type=array("", "kilo", "mega", "giga", "tera", "peta", "exa", "zetta", "yotta");
  $Index=0;
  while($Bytes>=1024)
  {
    $Bytes/=1024;
    $Index++;
  }
  return("".$Bytes." ".$Type[$Index]."bytes");
}
       */

    
    public function getFileExtension(){
        return $this->pathInfo['extension'];
    }
    
    public function getCutName(){
        return substr($this->getFullName(),0,25);
    }

    
    

    public function getParentDirectory(){
        return dirname($this->getDirName());
    }
    
    public function  isFile(){
        return is_file($this->getFullPath());
    }
    
    public function isDir(){
        return is_dir($this->getFullPath());
    }
    
    
    
    public function getDeviceNumber(){
        return $this->stat['dev'];
    }
    
    public function getINodoNumber(){
        return $this->stat['ino'];
    }
    
    public function getProteccionModeINodo(){
        return $this->stat['mode'];
    }
    
    public function getLinkNumber(){
        return $this->stat['nlink'];
    }
    
    public function getUserOwnerId(){
        return $this->stat['uid'];
    }
    
    public function getGroupOwnerId(){
        return $this->stat['gid'];
    }
    
    public function getDeviceTypeINodo(){
        return $this->stat['rdev'];
    }
    
    public function getDateLastAccess(){
        return $this->stat['atime'];
    }
    
    public function getDateLastModification(){
         return $this->stat['mtime'];
   }
   
   public function getDateLastModificacionINodo(){
       return $this->stat['ctime'];
   }
   
   
   
   public function getGroupInfo(){
       /*
        * http://php.net/manual/es/function.posix-getgrgid.php
        * Keys
        * name 
        * passwd
        * gid
        * members
        */
       return posix_getgrgid($this->getGroupOwnerId());
   }
   
   public function getUserInfo() {
       /**
        * http://php.net/manual/es/function.posix-getpwuid.php
        * 
        * keys:
        * name
        * passwd
        * uid
        * gid
        * gecos
        * dir    get home dir
        * shell get shell path
        */

       return posix_getpwuid($this->getUserOwnerId());
   }
   
   
   
   public function getDiskTotalSpace(){
       /*
        * http://php.net/manual/es/function.disk-total-space.php
        */
       return $this->human_filesize(disk_total_space($this->getFullPath()));
   }
   
   public function getDiskFreeSpace(){
       /*
        * http://php.net/manual/es/function.disk-free-space.php
        */
       return $this->human_filesize(disk_free_space($this->getFullPath()));
   }
   
    
   
   
   public function chmod(){
       /*
        * http://php.net/manual/es/function.chmod.php
        */

   }
   
   public function chown(){
       /*
        * http://php.net/manual/es/function.chown.php
        */

   }
   
   public function chgrp(){
       /*
        * http://php.net/manual/es/function.chgrp.php
        */

   }
   
   
   public function getPerms(){
       /**
        * http://php.net/manual/es/function.fileperms.php
        */

   }
   
   
   //http://php.net/manual/es/function.shell-exec.php
   //Simulador de shell
   
}
