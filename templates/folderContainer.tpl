<div id="folderContainer">
 
    <table id="folderContainerTableElements">
        <tr>
            <th>Name</th>
            <th>Size</th>
            <th>Date</th>
            <th>Type</th>
            
        </tr>
            {foreach $folderContent as $content}
                <tr id="{$content->getFullName()}" title="{$content->getFullName()}" class="{if $content@index is odd}colorEven1{else}colorOdd1{/if}">
                    <!-- Name -->
                    <td> {$content->getFullName()} </td>
                    <!-- Size -->
                    <td>
                        {if $content->isFile()}
                                {$content->getFileSize()}
                           {else}
                                ---
                       {/if}
                    </td>
                    <!-- Date -->
                    <td>{$content->getDateLastModification()|date_format:"%d/%m/%Y %H:%M"}</td>
                    <!-- Type -->
                    <td>
                       {if $content->isFile()}
                                {$content->getFileExtension()}
                           {else}
                                Directory
                       {/if}
                    </td>
                </tr>
                
            {/foreach}
    </table>
</div>
    
  