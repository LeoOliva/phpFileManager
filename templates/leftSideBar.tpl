<div id="treeSideBar">
    <ul class="nav">
                {foreach $folderContent as $content}
                    <li id="{$content->getFullName()}" title="{$content->getFullName()}">
                           {if $content->isDir()}
                               <a href="#">{$content->getCutName()}</a>
                           {/if}
                    </li>
                {/foreach}
    </ul>
</div>