<div id="folderContainerNavNar">
    <ul class="nav">
        <li>
            <div id="folderContainerToolBarDropdown1" class="dropdown">
                <a href="#" 
                   class="dropdown-toggle" 
                   data-toggle="dropdown" 
                   role="button" 
                   aria-haspopup="true" 
                   aria-expanded="false">Select 
                   <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="#">All</a></li>
                  <li><a href="#">None</a></li>
                  <li><a href="#">Inverse</a></li>
                </ul>
            </div>
        </li>
        <li><a>Delete</a></li>
        <li><a>Rename</a></li>
        <li><a>New folder</a></li>
        <li><a>Compress</a></li>
        <li><a>Download</a></li>
        <li><a>Upload</a></li>
    </ul>
</div>