<body>
    <!--
    Extra small (for smartphones .col-xs-*), 
    small (for tablets .col-sm-*), 
    medium (for laptops .col-md-*) 
    large (for laptops/desktops .col-lg-*). 
    These grid sizes enable you to control grid behavior 
    on different devices, i.e. desktop, laptops, tablet, smartphone, etc.
    -->
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-left">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">1</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">2</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">3</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">4</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">5</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">6</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">7</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">8</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">9</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">10</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #9999ff;">11</div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="background-color: #990033;">12</div>
            <br>
            <br>
            
            
             <div id="sidebar" class="col-lg-3 col-xs-10 sidebar-offcanvas"  role="navigation">
                 {include file="folderContainerToolBar.tpl"}
                 <!-- <hr> -->
                {include file="leftSideBar.tpl"}
                <!-- <hr> -->
            </div>
            <div id="content" class="col-lg-9 col-xs-10">
                {include file="serverInfo.tpl"}
                {include file="folderContainer.tpl"} 
            </div>
        </div>
    </div>
    
    
    
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/customView.js"></script>
    
</body>